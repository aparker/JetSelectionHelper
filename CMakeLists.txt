# The name of the package:
atlas_subdir (JetSelectionHelper)

# Add the shared library:
atlas_add_library (JetSelectionHelper
  JetSelectionHelper/*.h src/*.cxx
  PUBLIC_HEADERS JetSelectionHelper
  LINK_LIBRARIES xAODJet xAODBTagging)

